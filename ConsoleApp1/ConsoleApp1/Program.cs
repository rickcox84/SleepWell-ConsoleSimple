﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is you name?");
            string name = Console.ReadLine();

            Console.WriteLine("Hello," + name);

            Console.WriteLine("How many hours of sleep did you get?");
            int hoursOfSleep = int.Parse(Console.ReadLine());


            if (hoursOfSleep > 8)
            {
                Console.WriteLine("You are well rested!!");
            }
            else if (hoursOfSleep < 8)
            {
                Console.WriteLine("You need to get some more sleep!!!");
            }
            else
            {
                Console.WriteLine("You did something wrong," + name);
            }

            Console.Read();
        }
    }
}
